﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrikazLevela : MonoBehaviour {

    private bool check1_25 = false;
    private bool check26_50 = false;
    private GameObject prvih25;
    private GameObject sredina;
    private GameObject lijevo;
    private GameObject desno;
    private GameObject drugih25;



    // Use this for initialization
    void Start () {
        prvih25 = GameObject.Find("Prvi");
        drugih25 = GameObject.Find("Drugi");
        sredina = GameObject.Find("Sredina");
        desno = GameObject.Find("DesnaPozicija");
        lijevo = GameObject.Find("LijevaPozicija");
        prvih25.transform.position = sredina.transform.position;
        drugih25.transform.position = desno.transform.position;




	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("check1_25: " + check1_25);
        //Debug.Log("check26_50: " + check26_50);
        if (check1_25)
        {
            prvih25.transform.position = Vector3.Lerp(prvih25.transform.position, lijevo.transform.position, 3 * Time.deltaTime);
            drugih25.transform.position = Vector3.Lerp(drugih25.transform.position, sredina.transform.position, 3 * Time.deltaTime);
        }else if (check26_50)
        {
            drugih25.transform.position = Vector3.Lerp(drugih25.transform.position, desno.transform.position, 3 * Time.deltaTime);
            prvih25.transform.position = Vector3.Lerp(prvih25.transform.position, sredina.transform.position, 3 * Time.deltaTime);
        }
	}


    public void Lijevo()
    {
        check26_50 = true;
        check1_25 = false;
    }

    public void Desno()
    {
        check1_25 = true;
        check26_50 = false;
    }

    
}
