﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using admob;

public class LoseCanvas : MonoBehaviour {

    private BodoviController bodovi;
    private Text txt;

    // Use this for initialization
    void Start () {
        Debug.Log("start unity demo-------------");
        initAdmob();
        //bodovi = GameObject.FindObjectOfType<BodoviController>();
        //txt = GameObject.Find("WinBodovi").GetComponent<Text>();
        //txt.text = "pimpek";
        //Debug.Log("pamStart");

        if (ad.isRewardedVideoReady())
        {

            //nagrada
            ad.showRewardedVideo();
            //ad.loadRewardedVideo("ca-app-pub-3940256099942544/5224354917");
            Debug.Log("ne prikazi reklamu-------------");


        }
        else
        {
            //ad.showRewardedVideo();
            //ad.loadRewardedVideo("ca-app-pub-3940256099942544/5224354917");


            /* PUŠTANJE U POGON */
            //nasa reklama VIDEO
            ad.loadRewardedVideo("ca-app-pub-1114297404352848/5501953719");


            Debug.Log("da prikazi reklamu video-------------");
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Debug.Log(KeyCode.Escape + "-----------------");
        }
    }

    private void OnEnable()
    {
        bodovi = GameObject.FindObjectOfType<BodoviController>();
        txt = GameObject.Find("LoseBodovi").GetComponent<Text>();
        txt.text = bodovi.getBodovi().ToString();
        Debug.Log("pamEnable");
    }

    Admob ad;
    void initAdmob()
    {

        ad = Admob.Instance();
        //ad.bannerEventHandler += onBannerEvent;
        ad.interstitialEventHandler += onInterstitialEvent;
        ad.rewardedVideoEventHandler += onRewardedVideoEvent;
        //ad.nativeBannerEventHandler += onNativeBannerEvent;
        ad.initAdmob("ca-app-pub-3940256099942544/2934735716", "ca-app-pub-3940256099942544/4411468910");//all id are admob test id,change those to your
                                                                                                         //ad.setTesting(true);//show test ad
        ad.setGender(AdmobGender.MALE);
        string[] keywords = { "game", "crash", "male game" };
        //  ad.setKeywords(keywords);//set keywords for ad
        Debug.Log("admob inited -------------");

    }

    void onInterstitialEvent(string eventName, string msg)
    {
        Debug.Log("handler onAdmobEvent---" + eventName + "   " + msg);
        if (eventName == AdmobEvent.onAdLoaded)
        {
            Admob.Instance().showInterstitial();
        }
    }

    void onRewardedVideoEvent(string eventName, string msg)
    {
        Debug.Log("handler onRewardedVideoEvent---" + eventName + "  rewarded: " + msg);
    }
}
