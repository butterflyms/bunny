﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    private int odabir;
    private GameObject mama;
    private GameObject tata;
    private GameObject beba;
    private GameObject spawnPoint;

    private GameObject tataTrci;
    private GameObject mamaTrci;
    private GameObject bebaTrci;

    private Controller controller;
    private HealthController zivot;

    private bool win;
    private bool lose;
    private bool losePrviPut;

    private int brojac = 0;
    /*
     * referenca na controller
     * ako je win prikazati winMenu     
     * Bodovno stanje
     *  u on level was loaded sve na false win, losePrviPut, lose
     * ako je loseprviput prikazati loseMeni s tipkom za reklamu
     * ako je lose prikazati loseMenu bez tipke za reklamu 
     * 
     * brojac koliko puta je igubio na levelu
     * ako je igrac prvi put izgubio brojac se poveca za 1 ali ako igrac ne pogleda reklamu tj pritisne retry button
     * onda se brojac smanjuje za 1, ako igrac pogleda reklamu brojac ostaje na 1
     * 1 = izgubio je jednom
     * 0 = jos nije izgubio ili je izgubio ali nije pogledao reklamu
     * ako je igrac pobijedio onda brojac = 0 i win = true
     * povucemo bodovno stanje i prikazemo na canvasu
     * a dok je lose isto povucemo bodovno stanje i prikazemo na kanavasu
     * uz poruku neku motivacijsku tipa you can do it cmmoooon...
     * 
     
         
         */

    private void Update()
    {
        //Debug.Log(brojac);
        if(SceneManager.GetActiveScene().buildIndex >= 3)
        {
            if (win)
            {
                // prikazati win canvas
                controller.Win();
                brojac = 0;

            }else if (losePrviPut)
            {
                // prikazati lose canvas s mogucnosu pregleda video reklame za nastavak
                controller.LosePrviPut();
                
            }else if (lose)
            {
                // prikazati lose canvas bez mogucnosti pregleda video reklame za nastavak
                controller.LoseDrugiPut();
            }
        }
    }

    public void SetWin(bool winn)
    {
        win = winn;
        // u Controller-u u update staviti referencu na ovamu i provjeravati ako je win ili lose
        // ako je win pozvati funkciju koja prikazuje win canvas
        // i napisati funkcije za buttone na win i lose canvasu 
        //
        // ili odavde u controller
    }

    public bool GetWin()
    {
        return win;
    }

    public void SetLose()
    {
        brojac++;
        if (brojac < 2)
        {
            losePrviPut = true;
            lose = false;

        }
        else
        {
            losePrviPut = false;
            lose = true;
            

        }
    }

    public void LosePrviPut(bool a)
    {
        losePrviPut = a;
    }

    public void LoseDrugiPut(bool a)
    {
        lose= a;
    }

    public void Smanjibrojac()
    {
        brojac= 0;
    }



    private void OnLevelWasLoaded(int level)
    {
        Time.timeScale = 1f;
        //Debug.Log("LEvel: "+level);
        if (level == 2)
        {
            mama = GameObject.Find("Mama zeko");
            mama.SetActive(false);
            tata = GameObject.Find("Tata zeko");
            tata.SetActive(false);
            beba = GameObject.Find("Beba zeko");
            beba.SetActive(false);


            if(odabir == 0)
            {
                //tata
                tata.SetActive(true);
                
            }else if(odabir == 1)
            {
                //mama
                mama.SetActive(true);
            }
            else if(odabir == 2)
            {
                //beba
                beba.SetActive(true);
            }
        }else if(level > 2)
        {
            mamaTrci = GameObject.Find("Zekanica");
            mamaTrci.SetActive(false);
            tataTrci = GameObject.Find("Zekan");
            tataTrci.SetActive(false);
            bebaTrci = GameObject.Find("BebicaZekanica");
            bebaTrci.SetActive(false);
            controller = GameObject.FindObjectOfType<Controller>();
            zivot = FindObjectOfType<HealthController>();
            win = false;
            
            
            if (odabir == 0)
            {
                //tata
                tataTrci.SetActive(true);

            }
            else if (odabir == 1)
            {
                //mama
                mamaTrci.SetActive(true);
            }
            else if (odabir == 2)
            {
                //beba
                bebaTrci.SetActive(true);
            }
        }
    }


    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        //if(SceneManager.GetActiveScene().buildIndex >= 3)
        //{
        //    spawnPoint = GameObject.Find("Spawn");
        //}
    }

    public void postaviOdabir(int broj)
    {
        odabir = broj;
    }

    public int VratiOdabir()
    {
        return odabir;
    }


    //public void PostaviZajcaZaTrcanje()
    //{
    //    Instantiate(zajcikojiTrce[odabir], transform.position, Quaternion.identity);
    //}

    //public void LoadLevel(int level)
    //{
    //    SceneManager.LoadScene(level);
    //    zajcikojiTrce[odabir].SetActive(true);
    //    Debug.Log(spawnPoint);
    //    Debug.Log(zajcikojiTrce[odabir]);

    //}
}
