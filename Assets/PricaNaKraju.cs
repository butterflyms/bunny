﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PricaNaKraju : MonoBehaviour {

    private GameObject canvasPrica;
    private GameManager gm;


	// Use this for initialization
	void Start () {
        canvasPrica = GameObject.Find("PricaK");
        canvasPrica.SetActive(false);
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log(gm.GetWin());
        if (gm.GetWin())
        {
            canvasPrica.SetActive(true);                
        }
	}

    public void UgasiPricu()
    {
        canvasPrica.SetActive(false);
    }
}
