﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pratime : MonoBehaviour {

    public Transform[] target;
    private Vector3 offset;
    private GameManager gameManager;
    private int odabir;
    private float smoothSpeed = 0.15f;

    private float Vel = 0f;
    private float smoothTime = 0.125f;


    public bool izPracenjaPoYupracenjepoX = false;
    int brojac = 0;

	// Use this for initialization
	void Start () {
        gameManager = FindObjectOfType<GameManager>();
        //Debug.Log("Nasao");
        odabir = gameManager.VratiOdabir();
        offset = transform.position - target[odabir].position;
        //Debug.Log(offset);
    }

    //Vector3 newPos = new Vector3(0,-1);
    float newPos= -1f;
    bool a = false;

	// Update is called once per frame
	void FixedUpdate () {
        //Debug.Log(newPos);
        if (target[odabir].transform.position.y < 15f && target[odabir].transform.position.y > -3)
        {
            if (izPracenjaPoYupracenjepoX)
            {
                Vector3 desiredposition = target[odabir].position+offset;
                Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredposition, smoothTime);
                transform.position = smoothedPosition;
                //newPos = transform.position.y;




                Debug.Log("iz Y u X");
                brojac++;
                Debug.Log(brojac);
                if (brojac > 100)
                {
                    izPracenjaPoYupracenjepoX = false;
                    newPos = transform.position.y;                    
                    newPos -= 1.4f;
                    a = true;
                    
                }
            }
            else
            {
                if (a)
                {
                    transform.position = new Vector3(target[odabir].transform.position.x - 0.8f, newPos, 0f) + offset;
                }else if (!a)
                {
                    transform.position = new Vector3(target[odabir].transform.position.x, newPos, 0f) + offset;
                }
                //Debug.Log("iz X u X");

                

            }
            
        }
        else
        {
            Debug.Log("po Y i X");
            brojac = 0;
            Vector3 desiredposition = target[odabir].position + offset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredposition, smoothSpeed);
            transform.position = smoothedPosition;
            izPracenjaPoYupracenjepoX = true;

            a = false;

        }

    }
}
