﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OblaciLevel : MonoBehaviour {

    public Transform[] MaliOblaci;
    public Transform[] VelikiOblaci;

    public float brzinaMali = 0.005f;
    public float brzinaVeliki = 0.004f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
        foreach(Transform mt in MaliOblaci)
        {
            mt.position += new Vector3(brzinaMali, 0f);
        }

        foreach (Transform vt in VelikiOblaci)
        {
            vt.position += new Vector3(brzinaVeliki, 0f);
        }




    }
}
