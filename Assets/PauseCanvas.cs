﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseCanvas : MonoBehaviour {

    private BodoviController bodovi;
    private Text txt;
    private Text BrLevela;
	// Use this for initialization
	void Start () {
        txt = GameObject.Find("Bodovi").GetComponent<Text>();
        BrLevela = GameObject.Find("BrLevel").GetComponent<Text>();
        int brLevela = SceneManager.GetActiveScene().buildIndex - 2;
        BrLevela.text = brLevela.ToString();
    }


    private void OnEnable()
    {
        bodovi = FindObjectOfType<BodoviController>();
        txt = GameObject.Find("Bodovi").GetComponent<Text>();
        txt.text = bodovi.getBodovi().ToString();
        

        /*int br = SceneManager.GetActiveScene().buildIndex - 2;
        BrLevela.text = br.ToString();*/
    }




    // Update is called once per frame
    void Update () {
		
	}
}
