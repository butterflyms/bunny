﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    // u ovu varijablu idu sekunde nakon koojih bude se učitala početna scena
    public float timeToLoadNextLevel = 0f;
    

    // Use this for initialization
    void Start () {
        if (timeToLoadNextLevel <= 0f)
        {
            // ne radi nista, odnosno nemoj automatski ucitati novi level
        }
        else
        {
            //nakon timeToLoadNextLevel sekundi učitaj sljedeči level
            //ovo se koristi samo za početni intro
            Invoke("LoadNextLevel", timeToLoadNextLevel);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void LoadNextLevel()
    {
        // učitaj sljedecu scenu tj dohvati index aktivne scene dodaj 1 i ucitaj sljedecu po redu u build indexu
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    
  


    public void LoadLevel(string name)
    {
        //učitaj scenu prema imenu scene
        SceneManager.LoadScene(name);
    }
}
