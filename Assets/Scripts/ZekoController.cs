﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ZekoController : MonoBehaviour {

    public float speed;
    public Vector2 force;


    private Animator anim;
    private Rigidbody2D rgb;
    private bool grounded;
    private BodoviController bodovi;
    private HealthController zivot;
    private GameManager gameManager;
    private pratime pm;
    private MusicManager MM;
    private GameObject skok;
  

    // referenca na GameManager
    //napraviti brojac 
    // ako je zivot.getZivot() = 0 i brojac = 0 postaviti u game manageru losePrviPut na true nakon nastavka igrice postaviti lose PrviPut na false
    // ako je zivot.getZivot() = 0 i brojac = 1 postaviti u game manageru lose na true
    // ako se sudari s triggerom Win onda postaviti u game manager win na true

    // Use this for initialization
    void Start()
    {       
        rgb = GetComponent<Rigidbody2D>();
        force = new Vector2(0, 900f);
        anim = GetComponent<Animator>();
        bodovi = GameObject.FindObjectOfType<BodoviController>();
        zivot = GameObject.FindObjectOfType<HealthController>();
        gameManager = GameObject.FindObjectOfType<GameManager>();
        pm = FindObjectOfType<pratime>();
        MM = FindObjectOfType<MusicManager>();
        skok = GameObject.Find("proba");
    }


    void Update()
    {
        //CastRay();        

        if (Input.GetMouseButtonDown(0) && grounded)
        {
            if (CastRay())
            {
                rgb.AddForce(force);
                anim.SetTrigger("Skoci");
                grounded = false;
            }
           
        }
        if (gameManager.GetWin())
        {
            //spremi bodove
            // u PlayerPrefs se provjerava ako je novi high score ili ne ako nije onda se ne zapisuje
            PlayerPrefsManager.SetHighScore(bodovi.getBodovi(), SceneManager.GetActiveScene().buildIndex);
            // ovo uspjšno upisuje high score
            // odmah postavlja win na false da nebi bilo problem prilkom ucitavanja scene da nebi odma GameManager procitao da je win true
            gameManager.SetWin(false);
        }
    }
    //Update is called once per frame
    void FixedUpdate()
    {
        transform.position += Vector3.right * speed * Time.deltaTime;

    }



    //void Update()
    //{
    //    if (Input.GetMouseButtonDown(0))
    //    {
    //        Debug.Log("Pressed left click, casting ray.");
    //        CastRay();
    //    }
    //}

    bool CastRay()
    {
        bool a = false ;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);
        if (hit)
        {
            //Debug.Log(hit.collider.gameObject.name);
            //Debug.Log(hit.ToString());
            if (hit.collider.gameObject.tag == "Finish")
            {
                Debug.Log("Pimpek");                
                a = true;
                return a;
            }
            else
            {                
                a = false;
                return a;
            }

        }
        return a;
    }

      
    








    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "pod")
        {
            grounded = true;
        }        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bodovi")
        {
            Destroy(collision.gameObject);
            bodovi.UpdateBodovi(10);
            //pobrano jaje jer samo jaje daje bodove
            MM.Efekt(4);

            //premijestiti na skiptu za pick up jer prilikom skoka na pick up daje 20 bodova a ne 10
            //Destroy(collision.gameObject);            
        }
        else if (collision.gameObject.tag == "Zivot")
        {
            zivot.Zivot(-10);
            Destroy(collision.gameObject);
            // pobrano ono kaj nije dobro
            MM.Efekt(3);
            /*Collider2D c2d = gameObject.GetComponent<Collider2D>();
            Destroy(c2d);*/



        }
        else if(collision.gameObject.name == "Win")
        {
            gameManager.SetWin(true);
            Debug.Log("Win");
            //pobjeda
            MM.Efekt(0);
        }
        else if (collision.gameObject.tag == "Lose")
        {
            //pušiona
            MM.Efekt(1);
            zivot.Zivot(-zivot.KolicinaZivota());
            Destroy(collision.gameObject);
        }
        else if(collision.gameObject.tag == "ZivotUp")
        {
            //pick up život
            MM.Efekt(2);
            Destroy(collision.gameObject);
            if (zivot.KolicinaZivota() < 50)
            {
                zivot.Zivot(50);
                // ako je manje od 50 dodaj 50
            }
            else        
            {
                // ako je vise od 50 onda saznaj koliko je i oduzmi od 100 te onda dodaj toliko zivota da bude 100
                float kolicina = 100 - zivot.KolicinaZivota();
                zivot.Zivot(kolicina);
                
            }

        }else if(collision.gameObject.tag == "noJump")
        {
            skok.SetActive(false);
        }else if(collision.gameObject.tag == "activateJump")
        {
            skok.SetActive(true);
        }

    }


  


}
