﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Zvijezdice : MonoBehaviour {

    public Button[] gumbici;
    private int brojacZaGumbice = 0;
    public GameObject[] leveli;    
    private int brojac = 0;
    private int brojLeveli;

    private int brojLevelaZaGumbe = 4;
    
	// Use this for initialization
	void Start () {            
        Debug.Log("Leveli: " + leveli.Length);

        // prvo sve stavimo na false i kasnije uključimo prema bodovima
        foreach (GameObject g in leveli)
        {
            g.SetActive(false);            
        }

        

        // build index 1. levela = 3
        // build index 25. levela = 27
        // build index 50. levela = 52

        for (int i = 3; i <= 52; i++)
        {


            //Debug.Log("For:" +i);
            if (PlayerPrefsManager.GetHighScore(i) >= 400)
            {
                leveli[brojac].SetActive(true);
                leveli[brojac + 1].SetActive(true);
                leveli[brojac + 2].SetActive(true);
                //Debug.Log(leveli[brojac] + " : " + brojac);
                brojac += 3;
            }
            else if (PlayerPrefsManager.GetHighScore(i) <= 390 && PlayerPrefsManager.GetHighScore(i) >= 300)
            {
                leveli[brojac].SetActive(true);
                leveli[brojac + 1].SetActive(true);
                //Debug.Log(leveli[brojac] + " : " + brojac);
                brojac += 3;
            }
            else if (PlayerPrefsManager.GetHighScore(i) < 300 && PlayerPrefsManager.GetHighScore(i) > 200)
            {
                leveli[brojac].SetActive(true);
                //Debug.Log(leveli[brojac] + " : " + brojac);
                brojac += 3;

            }
            else
            {
                //Debug.Log(leveli[brojac] + " : " + brojac);
                brojac += 3;
            }

        }



        for (int y = 0; y< 50; y++)
        {
            gumbici[y].interactable = false;
            if (y == 0)
            {
                gumbici[y].interactable = true;
                continue;
            }

            if(PlayerPrefsManager.GetHighScore(brojLevelaZaGumbe) > 200)
            {
                gumbici[y].interactable = true;
                gumbici[y + 1].interactable = true;
            }
            else
            {
                if(PlayerPrefsManager.GetHighScore(brojLevelaZaGumbe-1) > 200)
                {
                    gumbici[y].interactable = true;
                }
            }
            brojLevelaZaGumbe++;

        }


    }

    // Update is called once per frame
    void Update () {
		
	}
}
