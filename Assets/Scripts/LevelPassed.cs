﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelPassed : MonoBehaviour {

    private Text txt;
    int brojLevela = 0;

	// Use this for initialization
	void Start () {
        txt = GetComponent<Text>();
        brojLevela = SceneManager.GetActiveScene().buildIndex - 2;
        txt.text = "Level " + brojLevela + " passed !";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
