﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingZajec : MonoBehaviour {

    public float speed;
    public Vector2 force;
    private Animator anim;

    private Rigidbody2D rgb;
	// Use this for initialization
	void Start () {
        rgb = GetComponent<Rigidbody2D>();
        force = new Vector2(0, 900f);
        anim = GetComponent<Animator>();
	}
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            rgb.AddForce(force);
            anim.SetTrigger("Skoci");
        }
    }
    // Update is called once per frame
    void FixedUpdate () {
        transform.position += Vector3.right * speed * Time.deltaTime;
        
    }
}
