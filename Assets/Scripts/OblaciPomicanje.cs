﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OblaciPomicanje : MonoBehaviour {

    public Transform[] velikiOblaci;
    public Transform[] maliOblaci;
    public float brzinaVeliki = 0.009f;
    public float brzinaMali = 0.005f;


    private Vector3 pocetnaPozicija;
    private Vector3 pocetnaMali;


	// Use this for initialization
	void Start () {
        pocetnaPozicija.x = -10.60f;
        pocetnaMali.x = -10.6f;        
	}
	
	// Update is called once per frame
	void Update () {
		for(int i = 0; i <velikiOblaci.Length; i++)
        {
            velikiOblaci[i].position += new Vector3(brzinaVeliki,0f,0f);
            if (velikiOblaci[i].position.x >= 11.07f)
            {
                pocetnaPozicija.y = velikiOblaci[i].position.y;
                velikiOblaci[i].position = pocetnaPozicija;
            }
        }

        for (int y = 0; y < maliOblaci.Length; y++)
        {
            maliOblaci[y].position += new Vector3(brzinaMali, 0f);
            if (maliOblaci[y].position.x >= 11.07f)
            {
                pocetnaMali.y = maliOblaci[y].position.y;
                pocetnaMali.z = 1f;
                maliOblaci[y].position = pocetnaMali;
            }
        }





    }




}
