﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OptionsController : MonoBehaviour {

    public GameObject optionsMenu;
    public GameObject CanvasOptionsiStory;


    //public Slider musicSlider;
    //public Slider effectsSlider;
    //public Slider graphicSlider;

    private Slider musicSlider;
    private Slider effectsSlider;
    private Slider graphicSlider;

    private MusicManager musicManager;
    //private Color crveno = Color.red;

	// Use this for initialization
	void Start () {
        optionsMenu.SetActive(false);
        musicManager = GameObject.FindObjectOfType<MusicManager>();
        //postavljanje slidera na trenutnu jačinu zvuka
        //musicSlider.value = PlayerPrefsManager.GetMasterVolume();
        //graphicSlider.value = PlayerPrefsManager.GetQuality();
        //effectsSlider.value = PlayerPrefsManager.GetEffectsVolume();
        
    }


    //private void OnLevelWasLoaded(int level)
    //{
    //    if(level == 1)
    //    {
    //        optionsMenu = GameObject.Find("OptionsMenu");
    //        CanvasOptionsiStory = GameObject.Find("Canvas");
            
    //        musicManager = GameObject.FindObjectOfType<MusicManager>();

    //        musicSlider = GameObject.Find("MusicSlider").GetComponent<Slider>();
    //        effectsSlider = GameObject.Find("EffectsSlider").GetComponent<Slider>();
    //        graphicSlider = GameObject.Find("GraphicSlider").GetComponent<Slider>();

    //        optionsMenu.SetActive(false);

    //        musicSlider.value = PlayerPrefsManager.GetMasterVolume();
    //        graphicSlider.value = PlayerPrefsManager.GetQuality();
    //        effectsSlider.value = PlayerPrefsManager.GetEffectsVolume();
            
    //    }
    //}



    private void Awake()
    {
        //na ovaj način pristupamo slider komponenti preko private varijable
        musicSlider = GameObject.Find("MusicSlider").GetComponent<Slider>();
        effectsSlider = GameObject.Find("EffectsSlider").GetComponent<Slider>();
        graphicSlider = GameObject.Find("GraphicSlider").GetComponent<Slider>();



        musicSlider.value = PlayerPrefsManager.GetMasterVolume();
        graphicSlider.value = PlayerPrefsManager.GetQuality();
        effectsSlider.value = PlayerPrefsManager.GetEffectsVolume();

    }

    // Update is called once per frame
    void Update () {
        if (SceneManager.GetActiveScene().buildIndex != 0)
        {
            musicManager.ChangeVolumeAndEffect(musicSlider.value, effectsSlider.value);
        }
        
	}


    public void SetActiveOptionsMenu()
    {
        Time.timeScale = 0;
        optionsMenu.SetActive(true);
        CanvasOptionsiStory.SetActive(false);
        //musicSlider.value = PlayerPrefsManager.GetMasterVolume();        
    }


    public void MakniOptionsMenu()
    {
        // ako kliknemo na x u options menu onda maknemo options menu i aktiviramo glavni canvas
        // i ne spremamo vrijednost zvuka nego vratimo na prijašnju odnosno pročitamo iz playerprefs koja je spremljena tamo
        Time.timeScale = 1;
        optionsMenu.SetActive(false);
        CanvasOptionsiStory.SetActive(true);
        musicSlider.value = PlayerPrefsManager.GetMasterVolume();
        graphicSlider.value = PlayerPrefsManager.GetQuality();
        effectsSlider.value = PlayerPrefsManager.GetEffectsVolume();

    }

    public void SpremiIMakniOptionsMenu()
    {
        // ako kliknemo na kvačicu onda maknemo options meni i aktiviramo glavni canvas
        // te spremimo vrijednost slidera koja je ima isti raspon kao i audio source pa oba mogu korisiti istu varijablu       
        Time.timeScale = 1;
        optionsMenu.SetActive(false);
        CanvasOptionsiStory.SetActive(true);
        //spremi zvuk
        PlayerPrefsManager.SetMasterVolume(musicSlider.value);
        //spremi graphic
        int a = (int)graphicSlider.value; // float to int
        SetQuality(a);
        //Spremi efekte
        PlayerPrefsManager.SetEffectsVolume(effectsSlider.value);
    
    }

    public void PovecajVrijednostMusic()
    {
        if (musicSlider.value >= 0 && musicSlider.value <= 0.91f)
        {
            musicSlider.value += 0.1f;
        }
    }


    public void SmanjiVrijednostMusic()
    {
        if (musicSlider.value >= 0f && musicSlider.value <= 1f)
        {
            musicSlider.value -= 0.1f;           
        }
    }

    public void PovecajVrijednostQuality()
    {
        if (graphicSlider.value <= 2)
        {
            graphicSlider.value += 1;
        }
    }

    public void SmanjiVrijednostQuality()
    {
        if (graphicSlider.value >= 1)
        {
            graphicSlider.value -= 1;
        }
    }

    public void PovecajVrijednostEffects()
    {
        if(effectsSlider.value >=0f && effectsSlider.value <= 0.91f)
        {
            effectsSlider.value += 0.1f;
        }
    }

    public void SmanjiVrijednostEffects()
    {
        if (effectsSlider.value >= 0f && effectsSlider.value <= 1f)
        {
            effectsSlider.value -= 0.1f;
        }
    }




    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
        PlayerPrefsManager.SetQuality(qualityIndex);
    }


    public void UgasiIgricu()
    {
        Application.Quit();
    }
}
