﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour {


    private float health;
    private Image prikazHealth;
    private GameManager gameManager;


	// Use this for initialization
	void Start () {
        health = 100;
        prikazHealth = GameObject.Find("Zivot").GetComponent<Image>(); 
        prikazHealth.fillAmount = (health / 100);
        //Debug.Log(prikazHealth);  
        gameManager = GameObject.FindObjectOfType<GameManager>();

        
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Zivot(float h)
    {
        if (health >= 10)
        {
            health += h;
            prikazHealth.fillAmount = health / 100;
            if(health <= 0)
            {
                gameManager.SetLose();
            }
        }
    }

    public void NakonReklameNapunizivot()
    {
        health = 100;
        prikazHealth.fillAmount = health / 100;
    }


    public  float KolicinaZivota()
    {
        return health;
    }


}
