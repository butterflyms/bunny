﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour {

    //5 canvasi Glavni(prikaz zivota, bodova, tipke, win, lose)
    private GameObject MainCanvas;
    private GameObject OptionsMenu;
    private GameObject PauseMenu;
    private GameObject WinMenu; 
    private GameObject LoseMenu;    

    //Buttoni za prikaz Pause i Options Menu-a
    private Button PauseButton;
    private Button OptionsButton;    

    //Pause Menu buttoni
    private Button PauseLevel;
    private Button PauseHome;
    private Button PauseRetry;
    private Button PauseResume;
    private Button PauseClose;

    //Options Menu buttoni
    private Button OptionsClose;
    private Button OptionsSaveAndClose;
    private Button OptionsMusicUp;
    private Button OptionsMusicDown;
    private Button OptionsEffectsUp;
    private Button OptionsEffectsDown;
    private Button OptionsGraphicUp;
    private Button OptionsGraphicDown;

    //WinMenu buttoni
    private Button WinLevel;    
    private Button WinRetry;    
    private Button WinNextLevel;


    //LoseMenu buttoni
    private Button LoseRetry;
    private Button LoseWatchAdAndContinue;
    private Button LoseLevel;



    //Slideri u Options Menu
    private Slider musicSlider, effectsSlider, graphicSlider;

    // Referenca na musicManager zbog promjene zvuka
    private MusicManager musicManager;

    private GameObject clickableArea;

    private HealthController healthController;

    private GameManager gameManager;




    // Use this for initialization
    void Start () {
        // pronađemo svih 5 canvasa i dodjelimo im reference na objekte
        MainCanvas = GameObject.Find("MainCanvas");        
        OptionsMenu = GameObject.Find("OptionsMenu");        
        PauseMenu = GameObject.Find("PauseMenu");
        WinMenu = GameObject.Find("WinCanvas");
        LoseMenu = GameObject.Find("LoseCanvas");
        
        // postavimo referencu na musicManager
        musicManager = GameObject.FindObjectOfType<MusicManager>();

        // postavimo reference na slidere
        musicSlider = GameObject.Find("MusicSlider").GetComponent<Slider>();
        effectsSlider = GameObject.Find("EffectsSlider").GetComponent<Slider>();
        graphicSlider = GameObject.Find("GraphicSlider").GetComponent<Slider>();

        healthController = GameObject.FindObjectOfType<HealthController>();
        gameManager = FindObjectOfType<GameManager>();


        PostaviReferenceNaButtone();
        PostaviFunkcijeNaButtone();


        //deaktiviramo options i pause jer se oni prikazuju samo dok se klikne na tipku
        OptionsMenu.SetActive(false);
        PauseMenu.SetActive(false);
        WinMenu.SetActive(false);
        LoseMenu.SetActive(false);

        clickableArea = GameObject.Find("proba");
        clickableArea.SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
        // prilikom promjene zvuka odmah želimo čuti promjenu
        musicManager.ChangeVolumeAndEffect(musicSlider.value, effectsSlider.value);
        //Debug.Log("Pauza Controller: " + zekoController.GetPauza());
        //if (!PauseButton.IsActive())
        //{
        //    k = false;
        //    zojec.SetActive(false);
        //}
    }

    private void PostaviReferenceNaButtone()
    {
        PauseButton = GameObject.Find("PauseButton").GetComponent<Button>();
        OptionsButton = GameObject.Find("SettingsButton").GetComponent<Button>();


        //Pause buttoni
        PauseLevel = GameObject.Find("Level").GetComponent<Button>();
        PauseHome = GameObject.Find("Pocetna").GetComponent<Button>();
        PauseRetry = GameObject.Find("Ponovi").GetComponent<Button>();
        PauseResume = GameObject.Find("Resume").GetComponent<Button>();
        PauseClose = GameObject.Find("PauseGasiButton").GetComponent<Button>();


        //Options buttoni
        OptionsSaveAndClose = GameObject.Find("PotvrdiButton").GetComponent<Button>();
        OptionsClose = GameObject.Find("GasiButton").GetComponent<Button>();
        OptionsMusicDown = GameObject.Find("MusicMinus").GetComponent<Button>();
        OptionsMusicUp = GameObject.Find("MusicPlus").GetComponent<Button>();
        OptionsEffectsUp = GameObject.Find("EffectsPlus").GetComponent<Button>();
        OptionsEffectsDown = GameObject.Find("EffectsMinus").GetComponent<Button>();
        OptionsGraphicUp = GameObject.Find("GraphicPlus").GetComponent<Button>();
        OptionsGraphicDown = GameObject.Find("GraphicMinus").GetComponent<Button>();


        //Win buttoni
        WinLevel = GameObject.Find("WinLevelScene").GetComponent<Button>();
        WinRetry = GameObject.Find("WinRetry").GetComponent<Button>();
        WinNextLevel = GameObject.Find("WinNextLevel").GetComponent<Button>();


        //Lose buttoni
        LoseRetry = GameObject.Find("LoseRetry").GetComponent<Button>();
        LoseWatchAdAndContinue = GameObject.Find("LoseContinueWithAd").GetComponent<Button>();
        LoseLevel = GameObject.Find("LoseLevelScene").GetComponent<Button>();
    }


    private void PostaviFunkcijeNaButtone()
    {
        PauseButton.onClick.AddListener(Pauza);
        OptionsButton.onClick.AddListener(Options);


        //Pause buttoni
        PauseLevel.onClick.AddListener(PauseLevelButton);
        PauseHome.onClick.AddListener(PauseHomeButton);
        PauseRetry.onClick.AddListener(PauseRetryButton);
        PauseResume.onClick.AddListener(PauseResumeButton);
        PauseClose.onClick.AddListener(PauseUgasiButton);


        //Options buttoni
        OptionsSaveAndClose.onClick.AddListener(OptionsSpremiIUgasi);
        OptionsClose.onClick.AddListener(OptionsUgasi);
        OptionsMusicDown.onClick.AddListener(OptionsSmanjiMusic);
        OptionsMusicUp.onClick.AddListener(OptionsPovecajMusic);
        OptionsEffectsDown.onClick.AddListener(OptionsSmanjiEffects);
        OptionsEffectsUp.onClick.AddListener(OptionsPovecajEffects);
        OptionsGraphicDown.onClick.AddListener(OptionsSmanjiGraphic);
        OptionsGraphicUp.onClick.AddListener(OptionsPovecajGraphic);


        //Win buttoni
        WinLevel.onClick.AddListener(WinLevelScena);
        WinRetry.onClick.AddListener(WinRetryLevel);
        WinNextLevel.onClick.AddListener(WinLoadNextLevel);


        //Lose buttoni
        LoseRetry.onClick.AddListener(LoseRetryFunction);
        LoseLevel.onClick.AddListener(LoseLevelScena);
        LoseWatchAdAndContinue.onClick.AddListener(PrikaziReklamuINastavi);            
    }



    public void Win()
    {
        Time.timeScale = 0f;
        WinMenu.SetActive(true);
        MainCanvas.SetActive(false);
    }


    public void LosePrviPut()
    {
        clickableArea.SetActive(false);
        Time.timeScale = 0f;
        LoseMenu.SetActive(true);
        MainCanvas.SetActive(false);

    }


    public void LoseDrugiPut()
    {
        clickableArea.SetActive(false);
        Time.timeScale = 0f;
        LoseMenu.SetActive(true);
        MainCanvas.SetActive(false);
        LoseWatchAdAndContinue.interactable = false;

    }
#region Funkcije za pause menu



    public void Pauza()
    {
        clickableArea.SetActive(false);
        Time.timeScale = 0;       
        MainCanvas.SetActive(false);
        PauseMenu.SetActive(true);        
    }

    private void PauseLevelButton()
    {
        
        Time.timeScale = 1;
        SceneManager.LoadScene(2);
    }

    private void PauseHomeButton()
    {
        
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }

    private void PauseRetryButton()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void PauseResumeButton()
    {
        clickableArea.SetActive(true);
        Time.timeScale = 1;
        PauseMenu.SetActive(false);
        MainCanvas.SetActive(true);
    }

    private void PauseUgasiButton()
    {
        clickableArea.SetActive(true);
        Time.timeScale = 1;
        PauseMenu.SetActive(false);
        MainCanvas.SetActive(true);
    }

    #endregion

#region Funkcije za Options Menu
    private void Options()
    {
        Time.timeScale = 0;
        clickableArea.SetActive(false);
        MainCanvas.SetActive(false);
        OptionsMenu.SetActive(true);
        musicSlider.value = PlayerPrefsManager.GetMasterVolume();
        graphicSlider.value = PlayerPrefsManager.GetQuality();
        effectsSlider.value = PlayerPrefsManager.GetEffectsVolume();
    }



    private void OptionsPovecajMusic()
    {
        if (musicSlider.value >= 0 && musicSlider.value <= 0.91f)
        {
            musicSlider.value += 0.1f;
        }
    }

    private void OptionsSmanjiMusic()
    {
        if (musicSlider.value >= 0f && musicSlider.value <= 1f)
        {
            musicSlider.value -= 0.1f;
        }
    }

    private void OptionsPovecajEffects()
    {
        if (effectsSlider.value >= 0f && effectsSlider.value <= 0.91f)
        {
            effectsSlider.value += 0.1f;
        }
    }

    private void OptionsSmanjiEffects()
    {
        if (effectsSlider.value >= 0f && effectsSlider.value <= 1f)
        {
            effectsSlider.value -= 0.1f;
        }
    }

    private void OptionsPovecajGraphic()
    {
        if (graphicSlider.value <= 2)
        {
            graphicSlider.value += 1;
        }
    }

    private void OptionsSmanjiGraphic()
    {
        if (graphicSlider.value >= 1)
        {
            graphicSlider.value -= 1;
        }
    }

    private void OptionsUgasi()
    {        
        Time.timeScale = 1;
        OptionsMenu.SetActive(false);
        MainCanvas.SetActive(true);
        musicSlider.value = PlayerPrefsManager.GetMasterVolume();
        effectsSlider.value = PlayerPrefsManager.GetEffectsVolume();
        graphicSlider.value = PlayerPrefsManager.GetQuality();
        clickableArea.SetActive(true);
    }

    private void OptionsSpremiIUgasi()
    {
        Time.timeScale = 1;
        OptionsMenu.SetActive(false);
        MainCanvas.SetActive(true);
        PlayerPrefsManager.SetMasterVolume(musicSlider.value);
        PlayerPrefsManager.SetEffectsVolume(effectsSlider.value);

        PlayerPrefsManager.SetQuality((int)graphicSlider.value);
        QualitySettings.SetQualityLevel((int)graphicSlider.value);
        clickableArea.SetActive(true);
    }

    #endregion

#region funkcije za Win Menu

    public void WinLevelScena()
    {
        //Debug.Log("why");
        //Time.timeScale = 1f;
        //MainCanvas.SetActive(true);        
        SceneManager.LoadScene("3_LevelScena");
    }
    
    public void WinRetryLevel()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void WinLoadNextLevel()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }


    #endregion

#region funkcije za LoseMenu
    public void LoseLevelScena()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("3_LevelScena");
        gameManager.LoseDrugiPut(false);
        gameManager.LosePrviPut(false);
        gameManager.SetWin(false);
        gameManager.Smanjibrojac();
    }


    public void LoseRetryFunction()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        gameManager.LosePrviPut(false);
        gameManager.LoseDrugiPut(false);
        gameManager.Smanjibrojac();
    }

    public void PrikaziReklamuINastavi()
    {
        Time.timeScale = 1;
        Debug.Log("reklama");
        MainCanvas.SetActive(true);
        LoseMenu.SetActive(false);
        clickableArea.SetActive(true);
        healthController.NakonReklameNapunizivot();       
        //gameManager.Smanjibrojac();        
        gameManager.LosePrviPut(false);       

    }
 


    #endregion


}
