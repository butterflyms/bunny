﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using admob;

public class WinCanvas : MonoBehaviour {


    private BodoviController bodovi;
    private Text txt;
    private Button NextLevel;




	// Use this for initialization
	void Start () {
        //bodovi = GameObject.FindObjectOfType<BodoviController>();
        //txt = GameObject.Find("WinBodovi").GetComponent<Text>();
        //txt.text = "pimpek";
        //Debug.Log("pamStart");
        Debug.Log("start unity demo-------------");
        initAdmob();

        //reklamica
        if (ad.isInterstitialReady())
        {
            ad.showInterstitial();
        }
        else
        {
            ad.loadInterstitial();
        }
    }


    //private void Awake()
    //{
    //    bodovi = GameObject.FindObjectOfType<BodoviController>();
    //    txt = GameObject.Find("WinBodovi").GetComponent<Text>();
    //    txt.text = bodovi.getBodovi().ToString();
    //    Debug.Log("pamAwake");

    //}

    private void OnEnable()
    {
        bodovi = GameObject.FindObjectOfType<BodoviController>();
        txt = GameObject.Find("WinBodovi").GetComponent<Text>();
        txt.text = bodovi.getBodovi().ToString();
        NextLevel = GameObject.Find("WinNextLevel").GetComponent<Button>();

        

        //Debug.Log("pamEnable");
    }

    //private void OnBecameVisible()
    //{
    //    txt.text = bodovi.getBodovi().ToString();
    //    Debug.Log("pamVisible");
    //}


    // Update is called once per frame
    void Update () {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Debug.Log(KeyCode.Escape + "-----------------");
        }

        if (bodovi.getBodovi() <= 200 && PlayerPrefsManager.GetHighScore(SceneManager.GetActiveScene().buildIndex) <= 200)
        {
            NextLevel.interactable = false;
        }

    }

    Admob ad;

    void initAdmob()
    {

        ad = Admob.Instance();
        //ad.bannerEventHandler += onBannerEvent;
        ad.interstitialEventHandler += onInterstitialEvent;
        ad.rewardedVideoEventHandler += onRewardedVideoEvent;
        //ad.nativeBannerEventHandler += onNativeBannerEvent;
        //ad.initAdmob("ca-app-pub-3940256099942544/2934735716", "ca-app-pub-3940256099942544/4411468910");//all id are admob test id,change those to your
        //ad.setTesting(true);//show test ad


        /* PUŠTANJE U POGON */
        //nas preko cijelog
        ad.initAdmob("ca-app-pub-1114297404352848/6407841275", "ca-app-pub-1114297404352848/6407841275");




        ad.setGender(AdmobGender.MALE);
        string[] keywords = { "game", "crash", "male game" };
        //  ad.setKeywords(keywords);//set keywords for ad
        Debug.Log("admob inited -------------");

    }
    void onInterstitialEvent(string eventName, string msg)
    {
        Debug.Log("handler onAdmobEvent---" + eventName + "   " + msg);
        if (eventName == AdmobEvent.onAdLoaded)
        {
            Admob.Instance().showInterstitial();
        }
    }

    void onRewardedVideoEvent(string eventName, string msg)
    {
        Debug.Log("handler onRewardedVideoEvent---" + eventName + "  rewarded: " + msg);
    }
}
