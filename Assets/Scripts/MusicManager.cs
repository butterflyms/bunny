﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour {

    public AudioClip[] listaZvukova;
    public AudioClip[] listaEfekata;


    public AudioSource audioSourceGlavni;
    public AudioSource audioSourceEfekt;
    private bool MainMenu;

  
    public void  Efekt(int koji)
    {
        audioSourceEfekt.clip = listaEfekata[koji];
        audioSourceEfekt.Play();
        audioSourceEfekt.loop = false;
    }



    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start () {
        audioSourceGlavni = GetComponent<AudioSource>();
        if(SceneManager.GetActiveScene().buildIndex == 0)
        {
            PlayerPrefsManager.SetMasterVolume(0.5f);
            audioSourceGlavni.volume = 0.5f;

            PlayerPrefsManager.SetQuality(2);



            PlayerPrefsManager.SetEffectsVolume(0.5f);
        }
        
	}

    private void OnLevelWasLoaded(int level)
    {
        audioSourceGlavni.volume = PlayerPrefsManager.GetMasterVolume();
        audioSourceEfekt.volume = PlayerPrefsManager.GetEffectsVolume();

        AudioClip thisLevelMusic = listaZvukova[level];
        if (thisLevelMusic)
        {
            audioSourceGlavni.clip = thisLevelMusic;
            audioSourceGlavni.loop = true;
            audioSourceGlavni.Play();            
        }
        else
        {
            Debug.Log("U trenutnom polju nema zvuka");
        }
    }

    public void ChangeVolumeAndEffect(float volume,float effect)
    {
        audioSourceGlavni.volume = volume;
        audioSourceEfekt.volume = effect;
        audioSourceGlavni.volume = PlayerPrefsManager.GetMasterVolume();
        audioSourceEfekt.volume = PlayerPrefsManager.GetEffectsVolume();
    }


    // Update is called once per frame
    void Update () {
		
	}
}
