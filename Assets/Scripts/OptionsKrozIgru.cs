﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsKrozIgru : MonoBehaviour {

    private GameObject optionsMenu;
    private GameObject leveliMenu;


    private Slider musicSlider;
    private Slider effectsSlider;
    private Slider graphicSlider;

    private MusicManager musicManager;
    // Use this for initialization
    void Start () {
        musicManager = GameObject.FindObjectOfType<MusicManager>();
    }
    private void Awake()
    {
        optionsMenu = GameObject.Find("OptionsMenu");
        Debug.Log(optionsMenu);
        




        musicSlider = GameObject.Find("MusicSlider").GetComponent<Slider>();
        effectsSlider = GameObject.Find("EffectsSlider").GetComponent<Slider>();
        graphicSlider = GameObject.Find("GraphicSlider").GetComponent<Slider>();



        musicSlider.value = PlayerPrefsManager.GetMasterVolume();
        graphicSlider.value = PlayerPrefsManager.GetQuality();
        effectsSlider.value = PlayerPrefsManager.GetEffectsVolume();
        optionsMenu.SetActive(false);
    }
    private void OnLevelWasLoaded(int level)
    {
        //Debug.Log(level);
        if(level == 2)
        {
            leveliMenu = GameObject.Find("Leveli");
            //Debug.Log(leveliMenu);
        }
    }
    // Update is called once per frame
    void Update () {
        musicManager.ChangeVolumeAndEffect(musicSlider.value, effectsSlider.value);
    }


    public void Prikazi()
    {
        Time.timeScale = 0;
        optionsMenu.SetActive(true);
        if(SceneManager.GetActiveScene().buildIndex == 2)
        {
            leveliMenu.SetActive(false);
        }
    }


    public void Odustani()
    {
        Time.timeScale = 1;
        optionsMenu.SetActive(false);
        if(SceneManager.GetActiveScene().buildIndex == 2)
        {
            leveliMenu.SetActive(true);
        }
        musicSlider.value = PlayerPrefsManager.GetMasterVolume();
        graphicSlider.value = PlayerPrefsManager.GetQuality();
        effectsSlider.value = PlayerPrefsManager.GetEffectsVolume();
    }

    public void Spremi()
    {
        Time.timeScale = 1;
        optionsMenu.SetActive(false);
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            leveliMenu.SetActive(true);
        }
        //spremi zvuk
        PlayerPrefsManager.SetMasterVolume(musicSlider.value);
        //spremi graphic
        int a = (int)graphicSlider.value; // float to int
        SetQuality(a);
        //Spremi efekte
        PlayerPrefsManager.SetEffectsVolume(effectsSlider.value);
    }


    public void PovecajVrijednostMusic()
    {
        if (musicSlider.value >= 0 && musicSlider.value <= 0.91f)
        {
            musicSlider.value += 0.1f;
        }
    }


    public void SmanjiVrijednostMusic()
    {
        if (musicSlider.value >= 0f && musicSlider.value <= 1f)
        {
            musicSlider.value -= 0.1f;
        }
    }

    public void PovecajVrijednostQuality()
    {
        if (graphicSlider.value <= 2)
        {
            graphicSlider.value += 1;
        }
    }

    public void SmanjiVrijednostQuality()
    {
        if (graphicSlider.value >= 1)
        {
            graphicSlider.value -= 1;
        }
    }

    public void PovecajVrijednostEffects()
    {
        if (effectsSlider.value >= 0f && effectsSlider.value <= 0.91f)
        {
            effectsSlider.value += 0.1f;
        }
    }

    public void SmanjiVrijednostEffects()
    {
        if (effectsSlider.value >= 0f && effectsSlider.value <= 1f)
        {
            effectsSlider.value -= 0.1f;
        }
    }




    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
        PlayerPrefsManager.SetQuality(qualityIndex);
    }

}
