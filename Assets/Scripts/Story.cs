﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Story : MonoBehaviour {


    private GameObject story;

	// Use this for initialization
	void Start () {
        story = GameObject.Find("Prica");
        story.SetActive(false);
	}

    public void PrikaziPricu()
    {
        story.SetActive(true);
    }


    public void ZatvoriPricu()
    {
        story.SetActive(false);
    }


}