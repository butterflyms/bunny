﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPrefsManager : MonoBehaviour
{

    const string MASTERVOLUMEKEY = "mastervolume";
    const string SOUNDEFFECTS = "effectsvolume";
    const string LEVELKEY = "level_unlocked";
    const string HIGHSCORE = "";
    const string QUALITY = "quality";


    public static void SetHighScore(float score, float levelindex)
    {
        if (score > PlayerPrefsManager.GetHighScore(SceneManager.GetActiveScene().buildIndex))
        {
            PlayerPrefs.SetFloat(HIGHSCORE + levelindex.ToString(), score);
            Debug.Log("ima=");
        }
        else
        {
            // ovo je samo za test
            //PlayerPrefs.SetFloat(HIGHSCORE + levelindex.ToString(), score);
            // ako score nije veci od od spremljenog scora onda nema high score   
            Debug.Log("nema");
        }
    }

    public static float GetHighScore(int level)
    {        
        return PlayerPrefs.GetFloat(HIGHSCORE + level.ToString());
    }


    public static float GetMasterVolume()
    {
        return PlayerPrefs.GetFloat(MASTERVOLUMEKEY);
    }

    public static void SetMasterVolume(float volume)
    {
        if(volume >= 0f && volume <= 1f)
        {
            PlayerPrefs.SetFloat(MASTERVOLUMEKEY, volume);
        }
        else
        {
            Debug.Log("nije moguce postaviti volume");
        }
    }

    public static void SetEffectsVolume(float effectVolume)
    {
        if(effectVolume >= 0 && effectVolume <= 1f)
        {
            PlayerPrefs.SetFloat(SOUNDEFFECTS, effectVolume);
        }
    }

    public static float GetEffectsVolume()
    {
        return PlayerPrefs.GetFloat(SOUNDEFFECTS);
    }



    public static void SetQuality(int amount)
    {
        PlayerPrefs.SetInt(QUALITY, amount);
    }

    public static int GetQuality()
    {
        return PlayerPrefs.GetInt(QUALITY);
    }
}
