﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BodoviController : MonoBehaviour {

    private int bodovi;
    private Text txt;

	// Use this for initialization
	void Start () {
        txt = GameObject.Find("Bodovii").GetComponent<Text>();
        bodovi = 0;
        txt.text = bodovi.ToString();
        //bodovi = 0;
        //Debug.Log(txt);        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int getBodovi()
    {
        return bodovi;
    }


    public void UpdateBodovi(int kolicina)
    {
        //bodovi += kolicina;        
        //txt.text = bodovi.ToString();


        StartCoroutine(Brojac(kolicina));
    }

    // YEAAAAAAA brojac jedan po jedan svaku 0.1 sekundu da izgleda ko brojcanik !! :D 

    IEnumerator Brojac(int kolicina)
    {
        for(int i = 1; i<=kolicina; i++)
        {
            bodovi += 1;
            txt.text = bodovi.ToString();
            yield return new WaitForSeconds(0.1f);
        }
    }
}
