﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OdabirZajca : MonoBehaviour {

    private GameManager gameManager;

    private Vector2 vec = new Vector2(1.3f, 1.3f); 
    private int broj = 0;
	// Use this for initialization
	void Start () {
        gameManager = FindObjectOfType<GameManager>();
    }



    // Update is called once per frame
    void Update () {
    
        
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Debug.Log("oce");
            transform.localScale = vec;           
            Debug.Log(gameObject.name);
            //ucitati sljedecu scenu
            if(gameObject.name == "Beba zeko")
            {
                gameManager.postaviOdabir(2);
            }else if(gameObject.name == "Mama zeko")
            {
                gameManager.postaviOdabir(1);
            }else if(gameObject.name  == "Tata zeko")
            {
                gameManager.postaviOdabir(0);
            }
            
            SceneManager.LoadScene("3_LevelScena");
            
        }
    }

    
}
